#pragma once
#include "DatosMemCompartida.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
int main()
{
	DatosMemCompartida *datosmem;
	int fd;
	fd=open("DatosComp.txt",O_RDWR);
	if(fd<0)
	{
		perror("Erroe en apretura de datos");
		exit(1);
	}
	datosmem=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(DatosMemCompartida),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0));

	if(datosmem==MAP_FAILED){
		perror("Error en proyeccion de fichero");
		close(fd);
		return (1);
	}
	close(fd);
	while(datosmem!=NULL){
		if(datosmem->esfera.centro.y>(datosmem->raqueta1.y1+datosmem->raqueta1.y2)/2)
			datosmem->accion=1;
		else if(datosmem->esfera.centro.y<(datosmem->raqueta1.y1+datosmem->raqueta1.y2)/2)
			datosmem->accion=-1;
		else if(datosmem->esfera.centro.y==(datosmem->raqueta1.y1+datosmem->raqueta1.y2)/2)
			datosmem->accion=0;
		usleep(25000);
	}

	unlink("DatosComp.txt");
}
	
	
